import { LiteralObject } from '@nestjs/common';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type MemberDocument = Member & Document;

@Schema()
export class Member {
  @Prop({ type: String, required: true })
  _id: string;

  @Prop({ type: Number, required: true })
  type: number;

  @Prop({ type: Number, required: true })
  status: number;

  @Prop({ type: Number, required: true })
  createdTime: number;

  @Prop({ type: Number, required: true })
  totalUnread: number;

  @Prop({ type: Number, required: true })
  lastActionTime: number;

  @Prop({ type: Number, required: true })
  lastReadTime: number;

  @Prop({ type: String, required: true })
  lastReadMessageId: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  payload: LiteralObject;
}

export const MemberSchema = SchemaFactory.createForClass(Member);
