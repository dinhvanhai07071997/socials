import { LiteralObject } from '@nestjs/common';
import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { MemberDocument, MemberSchema } from './member.shema';

export type ConversationDocument = Conversation & Document;
export type LastMessageDocument = LastMessage & Document;

@Schema()
export class LastMessage {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Message', required: true })
  _id: string;

  @Prop({ type: String, required: true })
  memberId: string;

  @Prop({ type: Number, required: true })
  type: number;

  @Prop({ type: String, required: true })
  content: string;

  @Prop({ type: Number, required: true })
  createdTime: number;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  payload: LiteralObject;
}

const lastMessageSchema = SchemaFactory.createForClass(LastMessage);

@Schema()
export class Conversation {
  @Prop({ type: Number, required: true })
  type: string;

  @Prop({ type: String })
  name: string;

  @Prop([{ type: MemberSchema }])
  members: MemberDocument[];

  @Prop({ type: Number, required: true })
  status: number;

  @Prop({ type: lastMessageSchema })
  lastMessage?: LastMessageDocument;

  @Prop({ type: Number, required: true })
  createdTime: number;

  @Prop({ type: Number })
  deletedTime?: number;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  payload: LiteralObject;
}

export const ConversationSchema = SchemaFactory.createForClass(Conversation);
