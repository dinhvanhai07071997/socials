import { Model } from 'mongoose';
import { Injectable, LiteralObject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Conversation, ConversationDocument } from './schemas/conversation.schema';
import { Exception } from '$helpers/exception';
import { CommonStatus, ErrorCode } from '$types/enums';
import { Message, MessageDocument } from './schemas/message.schema';
import { returnLoadMore } from '$helpers/utils';
import { MessageType } from '$app/shared/socket-io/enum';

interface ICreateConversation {
  name: string;
  type: Number;
  createdTime: Number;
  status: Number;
  members: Array<{
    _id: string;
    type: number;
    status: number;
    createdTime: number;
    totalUnread: number;
    lastActionTime: number;
    lastReadTime: number;
    lastReadMessageId: string;
    payload: LiteralObject;
  }>;
  payload: LiteralObject;
}

interface IFetchConversation {
  keyword?: string;
  type?: number;
  takeAfter: number;
  pageSize: number;
}
interface IFetchMessage {
  keyword?: string;
  /**For paging */
  takeAfter?: number;
  pageSize: number;
}

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Conversation.name) private conversationModel: Model<ConversationDocument>,
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
  ) {}

  async fetchMemberConversations(memberId: string, params: IFetchConversation) {
    const queryBuilder = this.conversationModel.aggregate();

    const condition = { lastMessage: { $exists: true }, 'members._id': { $eq: memberId } };

    if (params.keyword) {
      Object.assign(condition, { name: { $regex: new RegExp(params.keyword), $ss: 'i' } });
    }

    if (params.type) {
      Object.assign(condition, { type: { $eq: params.type } });
    }

    queryBuilder
      .match(condition)
      .addFields({
        member: {
          $filter: {
            input: '$members',
            as: 'member',
            cond: {
              $eq: ['$$member._id', memberId],
            },
          },
        },
      })
      .unwind('$member')
      .sort({ 'member.lastActionTime': -1 })
      .limit(params.pageSize);

    const results = await queryBuilder.project({
      _id: 1,
      deletedTime: 1,
      createdTime: 1,
      lastMessage: 1,
      status: 1,
      members: {
        _id: 1,
        status: 1,
        type: 1,
      },
      name: 1,
      type: 1,
      member: 1,
      payload: 1,
    });

    return returnLoadMore(results, params);
  }

  async createConversation(params: ICreateConversation) {
    const conversation = new this.conversationModel(params);

    const result = await conversation.save();

    return result;
  }

  async getConversationById(conversationId: string) {
    const result = await this.conversationModel.findById(conversationId);
    return result;
  }

  async getMessageById(messageId: string) {
    return await this.messageModel.findOne({
      messageId,
      _id: messageId,
    });
  }

  async countMessageUnread(conversationId: string, lastReadTime: number, messageType: MessageType[]) {
    return await this.messageModel.count({
      conversationId,
      status: CommonStatus.ACTIVE,
      createdTime: { $gt: lastReadTime },
      type: { $in: messageType },
    });
  }

  async updateConversation(conversationId: string, params: Conversation) {
    const conversation = await this.conversationModel.findById(conversationId);

    if (!conversation) {
      throw new Exception(ErrorCode.Not_Found, `Conversation id ${conversationId} not exist`);
    }

    Object.assign(conversation, params);

    const result = await conversation.save();

    return result;
  }

  async checkP2PConversationExists(firstMemberId: string, secondMemberId: string, type: number) {
    const conversation = await this.conversationModel.findOne(
      { type: type, 'members._id': { $and: [firstMemberId, secondMemberId] } },
      '_id',
    );

    return conversation;
  }

  async checkMemberOfConversation(memberId: string, conversationId: string): Promise<boolean> {
    const conversation = await this.conversationModel.findOne(
      { _id: conversationId, 'members._id': String(memberId) },
      '_id',
    );

    return !!conversation;
  }

  async saveMessage(params: Message) {
    const message = new this.messageModel(params);
    return await message.save();
  }

  async fetchMessage(conversationId: string, params: IFetchMessage) {
    const queryBuilder = this.messageModel.where({ conversationId });

    if (params.keyword) {
      queryBuilder.where({ content: { $regex: new RegExp(params.keyword), $options: 'i' } });
    }

    if (params.takeAfter) {
      queryBuilder.where({ createdTime: { $lt: params.takeAfter } });
    }

    queryBuilder.sort({ createdTime: -1 }).limit(Number(params.pageSize));

    const result = await queryBuilder.lean().exec();

    return returnLoadMore(result, params);
  }
}

/**
  const conversatiob = new this.conversationModel({
      type: 1,
      name: 'Demo 1',
      members: [
        {
          _id: '1',
          type: 1,

          status: 1,
          createdTime: 0,
          totalUnread: 0,
          lastActionTime: 11,
          lastReadTime: 0,
          lastReadMessageId: '1',
          payload: {},
        },
        {
          _id: '2',
          type: 1,
          status: 1,
          createdTime: 0,
          totalUnread: 0,
          lastActionTime: 12,
          lastReadTime: 0,
          lastReadMessageId: '1',
          payload: {},
        },
      ],
      status: 1,
      lastMessage: {
        _id: '6197f3813ae35a21579c2695',
        memberId: '1',
        type: 1,
        content: 'Hello friend!',
        createdTime: 0,
        payload: {},
      },
      createdTime: 0,
      deletedTime: 0,
      payload: {},
    });
    await conversatiob.save();

 */
