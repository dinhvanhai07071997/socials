# Microsoft Teams webhook library

Library for sending webhook messages to Microsoft Teams chanel

## How to install

1. In base project, create `microsoft-teams-webhook` library

```bash
$ nest g library microsoft-teams-webhook
```

2. Delete existing file

```bash
$ rm -rf libs/microsoft-teams-webhook
```

3. Clone our code

```bash
$ git clone git@git.amela.vn:hades-base/nestjs-api/microsoft-teams-webhook.git libs/microsoft-teams-webhook
```

4. Remove lib `.git`

```bash
$ rm -rf libs/microsoft-teams-webhook/.git
```

# How to use

Prepare URL [Send notification on a Microsoft Teams channel from a Data Factory pipeline](https://techcommunity.microsoft.com/t5/azure-data-factory-blog/send-notification-on-a-microsoft-teams-channel-from-a-data/ba-p/2456636#:~:text=Open%20Microsoft%20Teams%20and%20go,icon%20to%20identify%20your%20messages.)

1. Import

```Typescript
...
import { MicrosoftTeamsWebhookModule } from '@libs/microsoft-teams-webhook';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    ...
    MicrosoftTeamsWebhookModule.register(
      '{WEBHOOK_URL}',
    ),
    ...
  ],
  controllers: [...],
  providers: [...],
})
export class ExampleModule {}

```

2. Use

```Typescript
...
@Injectable()
export class ExampleService {
  constructor(
    ...
    private readonly microsoftTeamsWebhookService: MicrosoftTeamsWebhookService,
    ...
  ) {}

  async sendTextMessage() {
      // Send text message
      await this.microsoftTeamsWebhookService.sendMessage("Hello, this is message");
  }

  async sendWithTemplateOTP() {
      // Template OTP
      await this.microsoftTeamsWebhookService.sendOTP({
          env: "Test",
          action: "Forgot password",
          id: "truong.nguyen@amela.vn",
          otp: "123412",
          due: "2021-03-01T00:00:00.000Z"
      });
  }

  async sendRaw() {
      // Send without custom body
      await this.microsoftTeamsWebhookService.sendRaw({
          text: "This is raw message"
      });
  }

}

```
