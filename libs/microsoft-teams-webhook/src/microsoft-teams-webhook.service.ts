import { Inject, Injectable, LiteralObject } from '@nestjs/common';
import fetch from 'node-fetch';

interface ISendOTP {
  env: string;
  id: string;
  otp: string | number;
  due: string;
  action: string;
}
@Injectable()
export class MicrosoftTeamsWebhookService {
  constructor(@Inject('MICROSOFT_TEAMS_WEBHOOK_URL') private url: string) {}

  async sendRaw(body: LiteralObject) {
    return await fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    });
  }

  async sendMessage(message: string) {
    return await fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ text: message }),
    });
  }

  async sendOTP({ env, id, otp, due, action }: ISendOTP) {
    if (!this.url) throw new Error('Missing webhook url');

    return await fetch(this.url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        '@type': 'MessageCard',
        '@context': 'http://schema.org/extensions',
        themeColor: '0076D7',
        summary: action || 'Webhook OTP',
        sections: [
          {
            activityTitle: 'Webhook OTP',
            activitySubtitle: action || 'For testing',
            facts: [
              {
                name: 'Environment',
                value: env,
              },
              {
                name: 'Phone or Email',
                value: id,
              },
              {
                name: 'OTP',
                value: `\`${otp}\``,
              },
              {
                name: 'Due date',
                value: due,
              },
            ],
            markdown: true,
          },
        ],
        potentialAction: [],
      }),
    });
  }
}
