import { DynamicModule, Module } from '@nestjs/common';
import { MicrosoftTeamsWebhookService } from './microsoft-teams-webhook.service';

@Module({})
export class MicrosoftTeamsWebhookModule {
  static register(url: string): DynamicModule {
    return {
      module: MicrosoftTeamsWebhookModule,
      providers: [
        {
          provide: 'MICROSOFT_TEAMS_WEBHOOK_URL',
          useValue: url,
        },
        MicrosoftTeamsWebhookService,
      ],
      exports: [MicrosoftTeamsWebhookService],
    };
  }
}
