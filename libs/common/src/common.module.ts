import { Module } from '@nestjs/common';
import { ConfigModule, LanguageModule, ResourceModule, AuthorizationModule } from './index';
import { S3UploadModule } from './s3-upload';

@Module({
  imports: [ConfigModule, AuthorizationModule, ResourceModule, LanguageModule, S3UploadModule],
  controllers: [],
  exports: [ConfigModule, AuthorizationModule, ResourceModule, LanguageModule, S3UploadModule],
})
export class CommonModule {}
