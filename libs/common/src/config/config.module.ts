import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config.service';
import { ConfigController } from './config.controller';
import Config from './entities/Config';
import { APP_GUARD } from '@nestjs/core';
import { AdminGuard } from '$core/guards/admin.guard';

@Module({
  imports: [TypeOrmModule.forFeature([Config])],
  providers: [
    ConfigService,
    {
      provide: APP_GUARD,
      useClass: AdminGuard,
    },
  ],
  exports: [ConfigService],
  controllers: [ConfigController],
})
export class ConfigModule {}
