import { Module } from '@nestjs/common';
import { LanguageService } from './language.service';
import { LanguageController } from './language.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import Language from './entities/Language';
import LanguageEnv from './entities/LanguageEnv';
import LanguageKey from './entities/LanguageKey';
import LanguageTranslation from './entities/LanguageTranslation';
import { ConfigModule } from '@libs/common/config';
import { APP_GUARD } from '@nestjs/core';
import { AdminGuard } from '$core/guards/admin.guard';

@Module({
  imports: [TypeOrmModule.forFeature([Language, LanguageEnv, LanguageKey, LanguageTranslation]), ConfigModule],
  providers: [
    LanguageService,
    {
      provide: APP_GUARD,
      useClass: AdminGuard,
    },
  ],
  controllers: [LanguageController],
})
export class LanguageModule {}
