import { Module } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ResourceController } from './resource.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import Resource from './entities/Resource';
import { ConfigModule } from '@libs/common/config';
import { APP_GUARD } from '@nestjs/core';
import { AdminGuard } from '$core/guards/admin.guard';

@Module({
  imports: [TypeOrmModule.forFeature([Resource]), ConfigModule],
  providers: [
    ResourceService,
    {
      provide: APP_GUARD,
      useClass: AdminGuard,
    },
  ],
  controllers: [ResourceController],
})
export class ResourceModule {}
