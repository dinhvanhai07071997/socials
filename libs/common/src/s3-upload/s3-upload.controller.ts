import config from '$config';
import { Public } from '$core/decorators/public.decorator';
import { returnLoadMore } from '$helpers/utils';
import { Controller, Post, UploadedFiles } from '@nestjs/common';
import * as md5 from 'md5';
import { putImageToS3 } from './utils';

@Controller('upload')
export class S3UploadController {
  @Post('/')
  // @Public()
  async uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
    const results = [];

    for (const file of files) {
      const arr_ext = (file.originalname || '').split('.');
      const originalName = md5(file.originalname);

      const md5Name = arr_ext.length ? `${originalName}.${arr_ext[arr_ext.length - 1]}` : originalName;

      const fileName = `${Date.now().toString()}-${md5Name}`;

      await putImageToS3(file, fileName);
      results.push(fileName);
    }

    return returnLoadMore(results, {}, { domain: config.UPLOAD.S3_DOMAIN });
  }
}
