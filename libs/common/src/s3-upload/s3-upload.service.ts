import { Injectable } from '@nestjs/common';
import config from '$config';
import * as AWS from 'aws-sdk';
import * as Sharp from 'sharp';

const s3 = new AWS.S3({
  secretAccessKey: config.UPLOAD.S3_SECRET_KEY,
  accessKeyId: config.UPLOAD.S3_ACCESS_KEY,
  region: config.UPLOAD.S3_REGION,
});

@Injectable()
export class S3UploadService {
  async putImageToS3(image: Express.Multer.File, fileName: string) {
    await s3
      .putObject({
        ACL: 'public-read',
        Body: image.buffer,
        Bucket: config.UPLOAD.S3_BUCKET,
        ContentType: image.mimetype,
        Key: fileName,
      })
      .promise();

    if (image.originalname.search(/\.(gif|jpe?g|tiff|png|webp|bmp|svg|HEIC|blob)$/gi) !== -1) {
      await this.generateThumb(image, fileName);
      const putObjects = image['thumbs'].map((item) => {
        return s3
          .putObject({
            ACL: 'public-read',
            Body: item.buffer,
            Bucket: config.UPLOAD.S3_BUCKET,
            ContentType: image.mimetype,
            Key: item.fileName,
          })
          .promise();
      });

      await Promise.all(putObjects);
    }
  }

  async generateThumb(image: Express.Multer.File, fileName: string) {
    const thumbs = config.UPLOAD.THUMBS;

    for (const thumb of thumbs) {
      const [w, h] = thumb.split('x');
      let buffer = image.buffer;

      if (w && h) {
        buffer = await Sharp(image.buffer)
          .resize(Number(w), Number(h), {
            withoutEnlargement: true,
            fit: 'inside',
          })
          .toBuffer();

        if (!image['thumbs'] || !Array.isArray(image['thumbs'])) image['thumbs'] = [];

        image['thumbs'].push({
          fileName: `${w}x${h}/${fileName}`,
          buffer,
        });
      }
    }
  }
}
