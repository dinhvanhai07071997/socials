export enum ErrorCode {
  // Common error
  Unknown_Error = 'Unknown_Error',
  Invalid_Input = 'Invalid_Input',
  Not_Found = 'Not found',
  Token_Not_Exist = 'Token_Not_Exist',
  Forbidden_Resource = 'Forbidden_Resource',
  Unauthorized = 'Unauthorized',
  Too_Many_Requests = 'Too_Many_Requests',
  USER_INACTIVE = 'User_Inactive',
  PHONE_EXIT = 'Phone_exit',

  Email_Already_Exist = 'Email_Already_Exist',
  Email_Or_Password_Not_valid = 'Email_Or_Password_Not_valid',
  Resource_Already_Exists = 'Resource_Already_Exists',
  Can_Not_Disable_Default_language = 'Can_Not_Disable_Default_language',

  The_Allowed_Number_Of_Calls_Has_Been_Exceeded = 'The_Allowed_Number_Of_Calls_Has_Been_Exceeded',

  /**Message */
  Conversation_Not_Found = 'Conversation_Not_Found',
  Message_Not_Found = 'Message_Not_Found',
  You_Are_Not_Member_Of_This_Conversation = 'You_Are_Not_Member_Of_This_Conversation',
  You_Are_No_Longer_Active_In_This_Conversation = 'You_Are_No_Longer_Active_In_This_Conversation',

  user_have_been_activated = 'This user have been actived!',
}

export enum Environments {
  DEVELOPMENT = 'development',
  STAGING = 'staging',
  PRODUCTION = 'production',
}

export enum UserType {
  CLIENT = 'CLIENT',
  ADMIN = 'ADMIN',
  SUPER_ADMIN = 'SUPER_ADMIN',
}

export enum TokenType {
  ACCESS_TOKEN = 'ACCESS_TOKEN',
  REFRESH_TOKEN = 'REFRESH_TOKEN',
}

export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export enum Permissions {
  PERMISSION_MANAGEMENT = 1,
  CONFIG_MANAGEMENT = 2,
  RESOURCE_MANAGEMENT = 3,
  LANGUAGE_MANAGEMENT = 4,
}

export enum CommonStatus {
  ACTIVE = 1,
  INACTIVE = 0,
  NOT_VERIFY = 2,
  REJECTED = 3,
}

export enum MarrageStatus {
  SINGLE = 1,
  DATING = 2,
  MARRAGE = 3,
}

export enum UserGender {
  MALE = 1,
  FEMALE = 2,
  OTHER = 3,
}

export enum OtpType {
  REGISTER = 1,
  CHANGE_PASSWORD = 2,
  FORGET_PASSWORD = 3,
}

export enum ResourceType {
  TERM = 1,
  POLICY = 2,
  HELP = 3,
}

export enum RelationShipType {
  FOLLOWING = 1,
  FRIEND = 2,
  BLOCK = 3,
  CLOSE_FRIEND = 4,
}

export enum MediaType {
  IMAGE = 1,
  AUDIO = 2,
  VIDEO = 3,
  PDF = 4,
  STICKER = 5,
}

export enum ReactionType {
  LIKE = 1,
  LOVE = 2,
  CARE = 3,
  HAHA = 4,
  SAD = 5,
  ANGRY = 6,
}

export enum ImageUsage {
  AVATAR = 1,
  BANNER = 2,
  POST = 3,
  STORY = 4,
  MESSAGE = 5,
}

export enum NotificationTargetType {
  COMMON = 1,
  ALL = 2,
}
export enum NotificationType {
  COMMON = 1,
  ADMIN_SEND = 2,
}

export enum ReadNotification {
  UNREAD = 0,
  READ = 1,
}

export enum NotificationRedirectType {
  HOME = 1,
  POST_DETAIL = 2,
  PROFILE_MEMBER = 3,
  NOTIFICATION = 4,
}
export enum RedirectType {
  NOTIFICATION = 1,
}

export enum KeyQueue {
  PUSH_NOTIFICATION = 'PUSH_NOTIFICATION',
  ADMIN_PUSH_NOTIFICATION = 'ADMIN_PUSH_NOTIFICATION',
  SEND_MAIL = 'SEND_MAIL',
}

export enum VerificationCodeStatus {
  ACTIVE = 1,
  USED = 2,
  INACTIVE = 0,
}

export enum SearchType {
  USER = 1,
  PAGE = 2,
  GROUP = 3,
}

export enum PostAccess {
  GLOBAL = 1,
  FRIEND = 2,
  ONLY_ME = 3,
}
