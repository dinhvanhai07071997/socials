import { TokenType, UserType } from '$types/enums';

export interface ITokenPayload {
  id: number;
  userType: UserType;
  tokenType?: TokenType;
  [key: string]: any;
}

export interface IToken {
  token: string;
  refreshToken: string;
}
