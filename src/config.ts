export default {
  NODE_ENV: process.env.NODE_ENV,
  SERVER_PORT: Number(process.env.SERVER_PORT) || 3000,

  APP_NAME: 'Social',
  AUTH: {
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    JWT_ACCESS_TOKEN_EXPIRES_IN: process.env.JWT_ACCESS_TOKEN_EXPIRES_IN,
    JWT_REFRESH_TOKEN_EXPIRES_IN: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN,
    BCRYPT_HASH_ROUNDS: Number(process.env.BCRYPT_HASH_ROUNDS),
  },
  REDIS: {
    HOST: process.env.REDIS_HOST,
    PORT: Number(process.env.REDIS_PORT),
    DB: Number(process.env.REDIS_DB),
    PASSWORD: process.env.REDIS_PASSWORD,
  },
  MYSQL: {
    HOST: process.env.MYSQL_HOST,
    PORT: Number(process.env.MYSQL_PORT),
    USER: Number(process.env.MYSQL_USER),
    PASSWORD: Number(process.env.MYSQL_PASS),
    DBNAM: Number(process.env.MYSQL_DBNAME),
  },
  MONGO: {
    URI: process.env.MONGO_URI,
  },
  UPLOAD: {
    MAX_FILES: 10,
    S3_REGION: process.env.AWS_S3_REGION,
    S3_BUCKET: process.env.AWS_S3_BUCKET,
    S3_ACCESS_KEY: process.env.AWS_S3_ACCESS_KEY,
    S3_SECRET_KEY: process.env.AWS_S3_SECRET_KEY,
    S3_DOMAIN: `https://${process.env.AWS_S3_BUCKET}.s3.amazonaws.com`,
    THUMBS: ['', ...process.env.AWS_S3_THUMBS.split(' ').filter((item) => item)],
  },
  ONESIGNAL: {
    APP_ID: process.env.APP_ID,
    REST_KEY: process.env.REST_KEY,
  },
  SENDGRID: {
    API_KEY: process.env.SEND_GRID_API_KEY,
    EMAIL: process.env.SEND_GRID_EMAIL,
  },
};
