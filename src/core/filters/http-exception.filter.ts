import { ErrorCode } from '$types/enums';
import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { getLogger } from 'log4js';
const logger = getLogger('Exception');

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();
    const request = context.getRequest<Request>();

    Object.assign(exception, {
      request: {
        method: request.method,
        url: request.url,
        body: request.body,
        ip: request.ip,
        // authorization: request.headers?.authorization,
        user: request.user,
      },
    });
    logger.error(exception);

    const { statusCode, ...errorObject } = formatErrorObject(exception);

    response.status(statusCode).json(errorObject);
  }
}

export function formatErrorObject(exception: HttpException | any) {
  const errorObj = {
    success: false,
    statusCode: exception.status || HttpStatus.BAD_REQUEST,
    errorCode: ErrorCode.Unknown_Error,
    errorMessage: null,
  };

  if (exception instanceof HttpException) {
    const data = exception.getResponse() as any;
    if (data.error === 'Not Found') {
      return {
        success: false,
        statusCode: data.status || HttpStatus.BAD_REQUEST,
        errorCode: ErrorCode.Not_Found,
        errorMessage: data.message,
      };
    }

    if (data?.errorCode) errorObj.errorCode = data?.errorCode;
    if (data?.statusCode) errorObj.statusCode = data?.statusCode;
    if (data?.devMessage) errorObj['devMessage'] = data['devMessage'];
    if (data?.payload) errorObj['payload'] = data['payload'];

    if (data?.errorMessage) errorObj.errorMessage = data.errorMessage;

    if (data === 'ThrottlerException: Too Many Requests') {
      errorObj.errorCode = ErrorCode.The_Allowed_Number_Of_Calls_Has_Been_Exceeded;
      errorObj['devMessage'] = 'Too Many Requests';
    }
  }

  // TODO: Replace with real text
  // TODO: Get errorMessage from language
  if (!errorObj?.errorMessage) errorObj['errorMessage'] = errorObj.errorCode;

  return errorObj;
}
