import { CommonStatus, MarrageStatus, UserGender } from '$types/enums';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('user')
export default class User {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'email', type: 'varchar', length: 255, unique: true, nullable: true })
  email?: string;

  @Column({ name: 'password', type: 'varchar', length: 100, select: false })
  password?: string;

  @Column({
    name: 'status',
    type: 'tinyint',
    default: 2,
    comment: '1: Active, 0: Inactive, 2:Not verify, 3: Rejected ',
  })
  status?: CommonStatus;

  @Column({
    name: 'first_name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  firstName?: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    length: 255,
    nullable: false,
  })
  lastName?: string;

  @Column({
    name: 'gender',
    type: 'tinyint',
    comment: '1: single, 2: marriage, 3: dating',
    nullable: true,
  })
  gender?: UserGender;

  @Column({
    name: 'dob',
    type: 'date',
    nullable: true,
  })
  dob?: string;

  @Column({
    name: 'avatar',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  avatar?: string;

  @Column({
    name: 'banner',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  banner?: string;

  @Column({
    name: 'bio',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  bio?: string;

  @Column({
    name: 'address',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  address?: string;

  @Column({
    name: 'marrage_status',
    type: 'tinyint',
    comment: '1: single, 2: dating, 3: marriage',
    nullable: true,
  })
  marrageStatus?: MarrageStatus;

  @Column({
    name: 'is_online',
    type: 'boolean',
    nullable: true,
  })
  isOnline?: boolean;

  @Column({
    name: 'last_online',
    type: 'timestamp',
    nullable: true,
  })
  lastOnline?: boolean;

  @Column({ name: 'is_super_admin', type: 'tinyint', default: 0 })
  isSuperAdmin?: number;

  @Column({ name: 'role_id', type: 'tinyint', default: 0 })
  roleId: number;

  @Column({
    name: 'refresh_token',
    type: 'varchar',
    length: 500,
    nullable: true,
    select: false,
  })
  refreshToken?: string;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt?: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt?: string;
}
