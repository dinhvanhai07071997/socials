import { CommonStatus } from '$types/enums';
import {
  Column,
  Entity,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import Post from './Post';

@Entity('post_comment')
export default class PostComment {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'user_id', type: 'bigint', unsigned: true, nullable: false })
  memberId: number;

  @Column({ name: 'post_id', type: 'bigint', unsigned: true, nullable: false })
  postId: number;

  @Column({ name: 'status', type: 'tinyint', comment: '0: inactive, 1: Active', default: 1 })
  status: CommonStatus;

  @Column({ name: 'comment', type: 'varchar', length: 1000, nullable: false })
  comment: string;

  @Column({ name: 'deleted_at', type: 'datetime', nullable: true })
  deletedAt: string;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: string;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */
  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Post)
  @JoinColumn({ name: 'post_id', referencedColumnName: 'id' })
  post: Post;
}
