import { ReadNotification, CommonStatus } from '$types/enums';
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import User from './User';
import Notification from './Notification';

@Entity('notification_user')
export default class NotificationUser {
  @PrimaryColumn({ name: 'notification_id', type: 'bigint', unsigned: true })
  notificationId: number;

  @PrimaryColumn({ name: 'user_id', type: 'bigint', unsigned: true })
  userId: number;

  @Column('tinyint', {
    name: 'is_read',
    default: 0,
    comment: '0: unread, 1: read',
    nullable: true,
  })
  isRead: ReadNotification;

  @Column('tinyint', {
    name: 'status',
    default: 1,
    comment: '0: not active, 1: active',
    nullable: true,
  })
  status: CommonStatus;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: string;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @ManyToOne(() => Notification)
  @JoinColumn({ name: 'notification_id', referencedColumnName: 'id' })
  notification: Notification;
}
