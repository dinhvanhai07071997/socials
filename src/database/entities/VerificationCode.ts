import { OtpType, VerificationCodeStatus } from '$types/enums';
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('verification_code')
export default class VerificationCode {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'email', type: 'varchar', length: 255, nullable: false })
  email: string;

  @Column({
    name: 'type',
    type: 'tinyint',
    comment: '1: Register, 2: Change password, 3: foget password',
    nullable: false,
  })
  type: OtpType;

  @Column({ name: 'code', type: 'varchar', length: 6, nullable: false })
  code: string;

  @Column({ name: 'retry_count', type: 'tinyint', default: 0, comment: 'Max: 5' })
  retryCount: number;

  @Column({ name: 'status', type: 'tinyint', comment: '1: active, 0: inactive', default: 1 })
  status: VerificationCodeStatus;

  @Column({ name: 'expire_at', type: 'datetime', nullable: false })
  expireAt: Date;

  @Column({ name: 'use_at', type: 'datetime', nullable: true })
  useAt: string;

  @Column({ name: 'retry_at', type: 'datetime', nullable: true })
  retryAt: string;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: string;
}
