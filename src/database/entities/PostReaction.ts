import { CommonStatus, PostAccess } from '$types/enums';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import User from './User';

@Entity('post_reaction')
export default class PostReaction {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'post_id', type: 'bigint' })
  postId?: number;

  @Column({ name: 'status', type: 'tinyint', default: 1, comment: '1: active, 0: inactive' })
  status?: CommonStatus;

  @Column({ name: 'post_access', type: 'tinyint', comment: '1: Global, 2: Friends, 3: Only me' })
  postAccess: PostAccess;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt?: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt?: string;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */
  @ManyToOne(() => User)
  @JoinColumn({ name: 'owner_id', referencedColumnName: 'id' })
  user: User;
}
