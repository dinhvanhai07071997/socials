import { CommonStatus, NotificationTargetType } from '$types/enums';
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import NotificationUser from './NotificationUser';

@Entity('notification')
export default class Notification {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column('bigint', {
    name: 'created_by',
    comment: 'Actor of notification',
    nullable: true,
    unsigned: true,
  })
  createdBy?: number;

  @Column('bigint', {
    name: 'type',
    comment: 'Type of notificaiton',
    nullable: false,
  })
  type?: number;

  @Column({ type: 'bigint', name: 'target_type', nullable: false, default: 1 })
  targetType?: NotificationTargetType;

  @Column('bigint', {
    name: 'redirect_id',
    comment: 'Id to redirect',
    nullable: true,
    unsigned: true,
  })
  redirectId?: number;

  @Column('tinyint', {
    name: 'redirect_type',
    comment: 'Type to redirect',
    nullable: true,
  })
  redirectType?: number;

  @Column('varchar', {
    name: 'title',
    comment: 'Title of notificaiton',
    nullable: false,
    length: 500,
  })
  title?: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt?: string;

  @Column({ name: 'image', type: 'varchar', length: 255, nullable: true })
  image: string;

  @Column('tinyint', {
    name: 'status',
    default: 1,
    comment: '0: not active, 1: active',
    nullable: true,
  })
  status: CommonStatus;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */

  @OneToMany(() => NotificationUser, (notificationUser) => notificationUser.notification)
  notificationUser: NotificationUser[];
}
