import { CommonStatus, MediaType, SearchType } from '$types/enums';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import User from './User';

@Entity('search_history')
export default class Image {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'user_id', type: 'bigint', unsigned: true, nullable: false })
  userId: number;

  @Column({ name: 'keyword', type: 'varchar', length: 255, nullable: false })
  keyword: string;

  @Column({ name: 'status', type: 'tinyint', comment: '1: active, 0: inactive', default: 1 })
  status: CommonStatus;

  @Column({ name: 'search_type', type: 'tinyint', comment: '1: user, 2:page, 3:group', nullable: true })
  searchType: SearchType;

  @Column({ name: 'search_target_id', type: 'bigint', nullable: true })
  searchTargetId: number;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: string;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */
  @ManyToOne(() => User)
  @JoinColumn({ name: 'owner_id', referencedColumnName: 'id' })
  user: User;
}
