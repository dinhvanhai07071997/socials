import { CommonStatus, MediaType } from '$types/enums';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import User from './User';

@Entity('image')
export default class Image {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'owner_id', type: 'bigint', unsigned: true, nullable: false })
  memberId: number;

  @Column({ name: 'url', type: 'varchar', length: 255, nullable: false })
  name: string;

  @Column({ name: 'status', type: 'tinyint', comment: '1: active, 0: inactive', default: 1 })
  status: CommonStatus;

  @Column({ name: 'media_type', type: 'tinyint', comment: '1: active, 0: inactive', default: 1 })
  mediaType: MediaType;

  @Column({ name: 'image_usage', type: 'tinyint', comment: '1: AVATAR, 2: BANNER, 3: POST, 4: STORY' })
  imageUsage: CommonStatus;

  @Column({ name: 'deleted_at', type: 'datetime', nullable: true })
  deletedAt: string;

  @UpdateDateColumn({ name: 'update_at', type: 'datetime' })
  updateAt: string;

  @CreateDateColumn({ name: 'created_at', type: 'datetime' })
  createdAt: string;

  /* -------------------------------------------------------------------------- */
  /*                                  Relation                                  */
  /* -------------------------------------------------------------------------- */
  @ManyToOne(() => User)
  @JoinColumn({ name: 'owner_id', referencedColumnName: 'id' })
  user: User;
}
