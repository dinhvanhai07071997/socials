import config from '$config';
import { formatErrorObject } from '$core/filters/http-exception.filter';
import { LiteralObject } from '@nestjs/common';
import * as moment from 'moment';
import { first, head, last } from 'lodash';
import * as flatten from 'flat';

export function handleOutputPaging(data: any, totalItems: number, params, metadata = {}) {
  return {
    data,
    totalItems,
    pageIndex: params.pageIndex,
    totalPages: Math.ceil(totalItems / params.take),
    hasMore: data ? (data.length < params.take ? false : true) : false,
    ...metadata,
  };
}

export function handleInputPaging(params) {
  params.pageIndex = Number(params.pageIndex) || 1;
  params.take = Number(params.take) || 10;
  params.skip = (params.pageIndex - 1) * params.take;
  return params;
}

//! "When i wrote this code, only me and God knew how it works. Now only God knows..."
export function reformatFileLanguage(data: Array<any>, params: { code?: string; environment: string }) {
  const groupByLanguageCode = convertToObject(data, 'code');

  const languageObject = Object.keys(groupByLanguageCode).reduce((acc, cur) => {
    acc[cur] = groupByLanguageCode[cur].reduce((ac, cu) => {
      ac[cu.key] = cu.value;
      return ac;
    }, {});
    return acc;
  }, {});

  const result = flatten.unflatten(languageObject);
  if (params.code) {
    return result[params.code];
  }
  return result;
}

export function convertToObject(data: Array<any>, key: string): { [key: string]: Array<any> } {
  const result = {};
  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    const keyEl = element[key];
    if (!result[keyEl]) {
      result[keyEl] = [];
    }
    delete element[key];
    result[keyEl].push(element);
  }
  return result;
}

export function assignCachePrefix(key: string) {
  return `cache:${config.NODE_ENV}:${key}`;
}

export function socketFail(chanel: string, error: unknown, payload = {}) {
  const { statusCode, ...result } = { ...formatErrorObject(error), chanel, ...payload };
  return result;
}

export function returnPaging(data: LiteralObject, totalItems: number, params: LiteralObject, metadata = {}) {
  return {
    pageIndex: params.pageIndex,
    totalPages: Math.ceil(totalItems / params.take),
    totalItems,
    data,
    paging: true,
    metadata,
  };
}

export function returnLoadMore(data: LiteralObject, params: LiteralObject, metadata = {}) {
  return {
    paging: true,
    hasMore: data.length === params.pageSize,
    data,
    pageSize: params.pageSize,
    ...metadata,
  };
}

export function assignLoadMore(params: LiteralObject) {
  params.pageSize = Number(params.pageSize) || 10;

  return params;
}

export function assignPaging(params: LiteralObject) {
  params.pageIndex = Number(params.pageIndex) || 1;
  params.pageSize = Number(params.pageSize) || 10;
  params.skip = (params.pageIndex - 1) * params.take;

  delete params.pageSize;
  return params;
}

export const validateEmail = (email: string) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );
};

export const randomOTP = (length = 6): string => {
  const digits = '0123456789';
  const digitsLength = digits.length;
  let result = '';
  for (let i = 0; i < length; i++) {
    const index = Math.floor(Math.random() * digitsLength);
    result += digits[index];
  }
  return result;
};

/** handle assign domain url */
export function createImageUrl(img: string, w?: number, h?: number) {
  if (img && !w && !h && !img.startsWith('http')) return `${config.UPLOAD.S3_DOMAIN}/${img}`;
  if (img && img != '' && !img.startsWith('http')) return `${config.UPLOAD.S3_DOMAIN}/${String(w)}x${String(h)}/${img}`;
  return img;
}
function assignImageURL(data, key: string, thumb: boolean) {
  if (data[key].startsWith('http')) {
    data[key] = last(data[key].split('/'));
  }

  if (thumb) {
    const thumbs = config.UPLOAD.THUMBS;
    thumbs.forEach((el) => {
      const [w, h] = el.split('x');
      if (w && h) data[key + el] = createImageUrl(data[key], Number(w), Number(h));
    });
  }

  data[key] = createImageUrl(data[key]);
}
export function assignThumbURL(data: any, key: string, thumb = true) {
  if (!data) return data;
  if (Array.isArray(data)) {
    data.forEach((item) => {
      if (item && item[key]) assignImageURL(item, key, thumb);
    });
  } else {
    if (data[key] && data[key]) assignImageURL(data, key, thumb);
  }

  return data;
}

// Date Time Function
export function isBeforeTime(date: string | Date) {
  return moment().isBefore(date);
}
