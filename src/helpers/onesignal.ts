import { Client } from 'onesignal-node';
import { CreateNotificationBody } from 'onesignal-node/lib/types';
import config from '$config';
import { Environments } from '$types/enums';
import { PushNotificationData } from '$app/client/notification/dto/push-notification.dto';

const client = new Client(config.ONESIGNAL.APP_ID, config.ONESIGNAL.REST_KEY);
const memberTag = 'memberId';

function chunkArray(memberIds: number[], chunkSize: number) {
  let index = 0;
  const arrayLength = memberIds.length;
  const tempArray = [];

  for (index = 0; index < arrayLength; index += chunkSize) {
    const myChunk = memberIds.slice(index, index + chunkSize);
    // Do something if you want with the group
    tempArray.push(myChunk);
  }
  return tempArray;
}

function removeBlank(str: string) {
  return str.split(' ').join('');
}

export async function pushNotification(memberIds: number[], title: string, content: string, data: any) {
  if (!memberIds.length) return;

  const memberIdChunk = chunkArray(memberIds, 100);

  for (const memberIdsItem of memberIdChunk) {
    const filters = [];
    memberIdsItem.forEach((x) => {
      if (filters.length > 0) {
        filters.push({
          operator: 'OR',
        });
        filters.push({
          field: 'tag',
          key: memberTag,
          relation: '=',
          value: x,
        });
      } else {
        filters.push({
          field: 'tag',
          key: memberTag,
          relation: '=',
          value: x,
        });
      }
    });

    const notification: CreateNotificationBody = {
      headings: {
        en: title,
        ja: title,
        vi: title,
      },
      contents: {
        en: content,
        ja: content,
        vi: content,
      },
      data,
      android_group: `${removeBlank(config.APP_NAME)}_notification`,
      adm_group: `${removeBlank(config.APP_NAME)}_notification`,
      thread_id: `${removeBlank(config.APP_NAME)}_notification`,
      filters,
    };
    await client.createNotification(notification);
  }

  console.log('sendSuccess');
}
