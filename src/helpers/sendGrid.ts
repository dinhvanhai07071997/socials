// eslint-disable-next-line @typescript-eslint/no-var-requires
const sgMail = require('@sendgrid/mail');
import config from '$config';

sgMail.setApiKey(config.SENDGRID.API_KEY);

export async function sendEmail(receiver: string, subject: string, content: string, html: string) {
  const msg = {
    to: receiver,
    from: {
      name: 'Haireus facebook',
      email: config.SENDGRID.EMAIL,
    },
    subject: subject,
    text: content,
    html,
  };
  try {
    await sgMail.send(msg);
  } catch (error) {
    console.log(error.response.body);
  }
}
