import { PushNotificationData } from '$app/client/notification/dto/push-notification.dto';
import config from '$config';
import log from '$helpers/logger';
import { KeyQueue } from '$types/enums';
import { Queue, QueueEvents, QueueOptions, QueueScheduler, Worker } from 'bullmq';
import { pushNotification } from './onesignal';
import { sendEmail } from './sendGrid';

const logger = log('Queue');
const prefix = `bull_mq:${config.APP_NAME}:${config.NODE_ENV}`.split(' ').join('');

const connection = {
  host: config.REDIS.HOST,
  port: config.REDIS.PORT,
  password: config.REDIS.PASSWORD,
  db: config.REDIS.DB,
};

const queueOptions: QueueOptions = {
  connection,
  defaultJobOptions: {
    attempts: 3,
    backoff: {
      type: 'exponential',
      delay: 5000,
    },
  },
  prefix,
};

interface INotificationData {
  memberIds: number[];
  title: string;
  content: string;
  data: PushNotificationData;
}

export const SendNotificationSchedule = new QueueScheduler(KeyQueue.PUSH_NOTIFICATION, { prefix, connection });
export const SendNotificationQueue = new Queue<INotificationData>(KeyQueue.PUSH_NOTIFICATION, queueOptions);

export const SendNotificationWorker = new Worker(
  KeyQueue.PUSH_NOTIFICATION,
  async (job) => {
    if (job.name === KeyQueue.PUSH_NOTIFICATION) {
      const { title, content, data, memberIds } = job.data as INotificationData;
      await pushNotification(memberIds, title, content, data);
    }
  },
  { prefix, connection },
);
const SendNotifyEvent = new QueueEvents(KeyQueue.PUSH_NOTIFICATION, { prefix, connection });

SendNotifyEvent.on('waiting', (job) => {
  console.log(`Send notification - waiting: ${job.jobId}\n`);
});
SendNotifyEvent.on('drained', (job) => {
  console.log(`Send notification - drained\n`);
});
SendNotifyEvent.on('completed', (job) => {
  console.log(`Send notification - Complete: ${job.jobId}\n`);
});
SendNotifyEvent.on('failed', (job) => {
  console.error(`Send notification - JobId: ${job.jobId} failed.\n Reason: ${job.failedReason}\n`);
});

// Send mail queue

export const SendMailRetryFailReceiptScheduler = new QueueScheduler(KeyQueue.SEND_MAIL, { prefix, connection });

export const SendMailQueue = new Queue(KeyQueue.SEND_MAIL, queueOptions);

export const SendMailWorker = new Worker(
  KeyQueue.SEND_MAIL,
  async (job) => {
    if (job.name === KeyQueue.SEND_MAIL) {
      const { receiver, subject, content, html } = job.data;

      await sendEmail(receiver, subject, content, html);
    }
  },
  { prefix, connection },
);

const SendMailEvents = new QueueEvents(KeyQueue.SEND_MAIL, { prefix, connection });
// Log to test retry
SendMailEvents.on('waiting', (job) => {
  logger.info(`Send Email - waiting: ${job.jobId}\n`);
});

SendMailEvents.on('drained', (job) => {
  logger.info(`Send Email - drained\n`);
});

SendMailEvents.on('completed', (job) => {
  logger.info(`Send Email - Complete: ${job.jobId}\n`);
});

SendMailEvents.on('failed', (job) => {
  logger.error(`Send mail - JobId: ${job.jobId} failed.\n Reason: ${job.failedReason}\n`);
});
