import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoggerMiddleware } from '$core/middlewares/logger.middleware';
import { AllExceptionsFilter } from '$core/filters/http-exception.filter';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TransformResponseInterceptor } from '$core/interceptors/transform-res.interceptor';
import { JwtAuthGuard } from '$app/shared/auth/jwt-auth.guard';
import { SharedModule } from '$shared/shared.module';
import { AdminModule } from '$admin/admin.module';
import { ClientModule } from '$client/client.module';
import { PermissionsGuard, CommonModule } from '@libs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { MongooseModule } from '@nestjs/mongoose';
import config from '$config';
import { MessageModule } from '@libs/message';
import { ThrottlerStorageRedisService } from 'nestjs-throttler-storage-redis';

// https://docs.nestjs.com/recipes/sql-typeorm
@Module({
  imports: [
    TypeOrmModule.forRoot(),
    MongooseModule.forRoot(config.MONGO.URI),
    ScheduleModule.forRoot(),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 300,
      storage: new ThrottlerStorageRedisService(),
    }),
    AdminModule,
    ClientModule,
    SharedModule,
    CommonModule,
    MessageModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformResponseInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: PermissionsGuard,
    },
  ],
  exports: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
