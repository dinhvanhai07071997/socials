import { Exception } from '$helpers/exception';
import { socketFail } from '$helpers/utils';
import { ErrorCode } from '$types/enums';
import { Catch, ArgumentsHost } from '@nestjs/common';
import { BaseWsExceptionFilter } from '@nestjs/websockets';

@Catch()
export class WsExceptionsFilter extends BaseWsExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const [, , fn] = host.getArgs();

    if (typeof fn === 'function') {
      const handler = exception?.error?.handler || 'Unknown';
      const errorCode = exception?.error?.errorCode || ErrorCode.Unknown_Error;
      const err = socketFail(`Server: ${handler}`, new Exception(errorCode));
      fn(err);
    }
    super.catch(exception, host);
  }

  handleError<TClient extends { emit: Function }>(client: TClient, exception: any): void {
    super.handleError(client, exception);
  }

  handleUnknownError<TClient extends { emit: Function }>(exception: any, client: TClient): void {
    super.handleUnknownError(exception, client);
  }
}
