export enum EventKey {
  JOIN_CONVERSATION = 'joinConversation',
  SEND_MESSAGE = 'sendMessage',
  FETCH_MESSAGE = 'fetchMessage',
  FETCH_CONVERSATION = 'fetchConversation',
  GET_OR_CREATE_P2P_CONVERSATION = 'getOrCreateP2PConversation',
  MESSAGE = 'message',
  CONVERSATION = 'conversation',
  EXCEPTION = 'exception',
}

export enum MessageType {
  TEXT = 1,
}

export enum ConversationMemberStatus {
  ACTIVE = 1,
  INACTIVE = 0,
}

export enum ConversationMemberType {
  COMMON = 1,
  ADMIN = 2,
}

export enum ConversationType {
  P2P = 1,
  GROUP = 2,
}
