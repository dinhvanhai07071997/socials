import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { JoinConversationDto } from './dto/joinConversation.dto';
import { MessageService } from '@libs/message/message.service';
import { Cache } from 'cache-manager';
import { Exception } from '$helpers/exception';
import { CommonStatus, ErrorCode } from '$types/enums';
import { SendMessageDto } from './dto/sendMessage.dto';
import { assignLoadMore, assignPaging } from '$helpers/utils';
import { ConversationMemberStatus, ConversationMemberType, ConversationType, MessageType } from './enum';

@Injectable()
export class SocketIOService {
  private readonly logger = new Logger(SocketIOService.name);

  constructor(private readonly messageService: MessageService, @Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  generateKeyCacheForCheckMember(memberId: string, conversationId: string) {
    return `socket:isMemberOfConversation:${conversationId}:${memberId}`;
  }

  async checkMemberOfConversation(memberId: string, conversationId: string) {
    const keyCache = this.generateKeyCacheForCheckMember(memberId, conversationId);

    const cacheData = await this.cacheManager.get(keyCache);
    if (cacheData) return true;

    const isMember = await this.messageService.checkMemberOfConversation(memberId, conversationId);

    await this.cacheManager.set(keyCache, !!isMember);

    return !!isMember;
  }

  async joinConversation({ id, clientId }: JoinConversationDto) {
    const isMember = await this.checkMemberOfConversation(clientId, id);
    if (!isMember) {
      const devMessage = "You are trying to join a conversation, but you're not part of it.";
      throw new Exception(ErrorCode.You_Are_Not_Member_Of_This_Conversation, devMessage);
    }
    return;
  }

  async readMessage(memberId: string, messageId: string) {
    const message = await this.messageService.getMessageById(messageId);
    if (!message) {
      throw new Exception(ErrorCode.Message_Not_Found, 'Can not found message');
    }

    const conversation = await this.messageService.getConversationById(message.conversationId);

    const member = conversation.members.find((item) => String(item._id) === memberId);

    if (member.status !== ConversationMemberStatus.ACTIVE) {
      // throw new Exception(ErrorCode.You_Are_No_Longer_Active_In_This_Conversation);
      return;
    }
    const currentTime = Date.now();

    const totalUnread = await this.messageService.countMessageUnread(message.conversationId, currentTime, [
      MessageType.TEXT,
    ]);

    member.totalUnread = totalUnread;
    member.lastReadTime = currentTime;
    member.lastReadMessageId = String(message._id);

    await conversation.save();
  }

  async sendMessage(params: SendMessageDto) {
    const isMember = await this.checkMemberOfConversation(params.memberId, params.conversationId);
    if (!isMember) {
      const devMessage = "You are trying to join a conversation, but you're not part of it.";
      throw new Exception(ErrorCode.You_Are_Not_Member_Of_This_Conversation, devMessage);
    }

    const conversation = await this.messageService.getConversationById(params.conversationId);

    const member = conversation.members.find((item) => String(item._id) === params.memberId);
    if (member.status !== ConversationMemberStatus.ACTIVE) {
      throw new Exception(ErrorCode.You_Are_No_Longer_Active_In_This_Conversation);
    }

    const createdTime = Date.now();

    const message = await this.messageService.saveMessage({
      ...params,
      status: CommonStatus.ACTIVE,
      createdTime: createdTime,
    });

    try {
      /**
       * Update last message info for conversation
       */
      Object.assign(conversation.lastMessage, {
        _id: message._id,
        memberId: message.memberId,
        type: message.type,
        content: message.content,
        createdTime: message.createdTime,
        payload: message.payload || {},
      });

      /**
       * 1. Update lastActionTime for all active member
       * 2. Increase for totalUnread of all active member (except sender.)
       * 3. Update lastReadTime, lastReadMessageId for sender
       */
      conversation.members.map((item) => {
        if (item.status === ConversationMemberStatus.ACTIVE) {
          item.lastActionTime = createdTime;

          if (String(item._id) !== params.memberId) {
            if (!item.totalUnread) item.totalUnread = 0;

            item.totalUnread = Number(item.totalUnread) + 1;
          }

          if (String(item._id) === params.memberId) {
            item.lastReadTime = createdTime;
            item.lastReadMessageId = message._id;
          }
        }
      });

      const _conversation = await conversation.save();
      const _message = message.toObject();

      return { conversation: _conversation.toObject(), message: _message };
    } catch (error) {
      await message.delete();
      throw error;
    }
  }

  async fetchMessage(memberId: string, conversationId: string, params) {
    const isMember = await this.checkMemberOfConversation(memberId, conversationId);
    if (!isMember) {
      const devMessage = "You are trying to join a conversation, but you're not part of it.";
      throw new Exception(ErrorCode.You_Are_Not_Member_Of_This_Conversation, devMessage);
    }

    assignLoadMore(params);
    const messages = await this.messageService.fetchMessage(conversationId, params);

    return messages;
  }

  async fetchMemberConversation(memberId: string, params) {
    assignLoadMore(params);
    const messages = await this.messageService.fetchMemberConversations(memberId, params);

    return messages;
  }

  async getOrCreateP2PConversation(memberId: string, targetId: string) {
    const conversation = await this.messageService.checkP2PConversationExists(memberId, targetId, ConversationType.P2P);
    if (conversation) {
      return conversation;
    }

    const createdTime = Date.now();

    const newConversation = await this.messageService.createConversation({
      name: '',
      type: ConversationType.P2P,
      createdTime: createdTime,
      status: CommonStatus.ACTIVE,
      members: [
        {
          _id: memberId,
          type: ConversationMemberType.COMMON,
          status: ConversationMemberStatus.ACTIVE,
          createdTime: createdTime,
          totalUnread: 0,
          lastActionTime: null,
          lastReadTime: null,
          lastReadMessageId: null,
          payload: {},
        },
        {
          _id: targetId,
          type: ConversationMemberType.COMMON,
          status: ConversationMemberStatus.ACTIVE,
          createdTime: createdTime,
          totalUnread: 0,
          lastActionTime: null,
          lastReadTime: null,
          lastReadMessageId: null,
          payload: {},
        },
      ],
      payload: {},
    });

    return { _id: newConversation._id };
  }
}
