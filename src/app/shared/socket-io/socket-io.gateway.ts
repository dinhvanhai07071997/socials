import { WebSocketGateway, SubscribeMessage, WebSocketServer, MessageBody } from '@nestjs/websockets';
import { SocketIOService } from './socket-io.service';
import { Socket, Server, Namespace } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { LiteralObject, Logger, UseFilters, UseGuards } from '@nestjs/common';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';
import { JoinConversationDto } from './dto/joinConversation.dto';
import { EventKey } from './enum';
import { WsThrottlerGuard } from './ws-thottler.guard';
import { Throttle } from '@nestjs/throttler';
import { socketFail } from '$helpers/utils';
import { WsExceptionsFilter } from './ws-exception.filter';
import { SendMessageDto } from './dto/sendMessage.dto';
import { ErrorCode } from '$types/enums';
import { validate } from '$helpers/validate';
import { joinConversationSchema } from './schema/joinConversation.schema';
import { sendMessageSchema } from './schema/sendMessage.schema';

export type TSocketData = {
  /**Member id */
  id: string;
};

@Throttle(1000, 60)
@UseGuards(WsThrottlerGuard)
@UseFilters(new WsExceptionsFilter())
@WebSocketGateway({ namespace: '/' })
export class SocketIOGateway {
  @WebSocketServer()
  server: Namespace<DefaultEventsMap, DefaultEventsMap, DefaultEventsMap, TSocketData>;

  private readonly logger = new Logger(SocketIOGateway.name);

  constructor(private readonly socketIOService: SocketIOService, private readonly authService: AuthService) {}

  afterInit() {
    /**
     * User connect lên server gửi kèm header bên trong chứa authorization token.
     * 1. Nếu token hợp lệ thì next()
     * 2. Nếu token không hợp lệ thì trả về lỗi và không cho người dung connect.
     *  - message lỗi sẽ được gửi về event connect_error Unauthorized
     */
    this.server.use((client, next) => {
      try {
        const payload = this.authService.verifyAccessToken(client.handshake.headers.authorization);
        if (!payload) {
          return next(Object.assign(new Error(ErrorCode.Unauthorized)));
        }

        Object.assign(client.data, { id: payload.id });
        return next();
      } catch (error) {
        this.logger.error(error);
        return next(new Error(error.message));
      }
    });
  }

  generateMemberRoom(id: number | string) {
    return `member_${id}`;
  }

  generateConversationRoom(id: string) {
    return `conversation_${id}`;
  }

  async handleConnection(client: Socket) {
    client.removeAllListeners();
    client.join(this.generateMemberRoom(client.data.id));
  }

  @SubscribeMessage(EventKey.JOIN_CONVERSATION)
  async joinConversation(client: Socket, data: JoinConversationDto) {
    try {
      validate(joinConversationSchema, data);

      this.socketIOService.joinConversation({ ...data, clientId: client.data.id });

      const roomName = this.generateConversationRoom(data.id);
      client.join(roomName);

      this.logger.debug(`memberId: "${client.data.id}" joinConversation "${roomName}"`);

      return this.success(EventKey.JOIN_CONVERSATION);
    } catch (error) {
      this.logger.error(error);
      return socketFail(EventKey.JOIN_CONVERSATION, error);
    }
  }

  @SubscribeMessage(EventKey.SEND_MESSAGE)
  async sendMessage(client: Socket, data: SendMessageDto) {
    try {
      const clientId = String(client.data.id);
      validate(sendMessageSchema, data);

      const { conversation, message } = await this.socketIOService.sendMessage({ ...data, memberId: clientId });

      const roomName = this.generateConversationRoom(data.conversationId);
      client.to(roomName).emit(EventKey.MESSAGE, message);

      conversation.members.forEach((item) => {
        const room = this.generateMemberRoom(client.data.id);
        this.server.to(room).emit(EventKey.CONVERSATION, { ...conversation, member: item });
      });

      return this.success(EventKey.SEND_MESSAGE, message);
    } catch (error) {
      this.logger.error(error);
      return socketFail(EventKey.SEND_MESSAGE, error);
    }
  }

  @SubscribeMessage(EventKey.FETCH_MESSAGE)
  async fetchMessage(client: Socket, { conversationId, ...data }) {
    try {
      const result = await this.socketIOService.fetchMessage(String(client.data.id), conversationId, data);

      return this.success(EventKey.FETCH_MESSAGE, result);
    } catch (error) {
      this.logger.error(error);
      return socketFail(EventKey.FETCH_MESSAGE, error);
    }
  }

  @SubscribeMessage(EventKey.GET_OR_CREATE_P2P_CONVERSATION)
  async getOrCreateP2PConversation(client: Socket, { targetId }) {
    try {
      const result = await this.socketIOService.getOrCreateP2PConversation(String(client.data.id), targetId);

      return this.success(EventKey.GET_OR_CREATE_P2P_CONVERSATION, result);
    } catch (error) {
      this.logger.error(error);
      return socketFail(EventKey.GET_OR_CREATE_P2P_CONVERSATION, error);
    }
  }

  @SubscribeMessage(EventKey.FETCH_CONVERSATION)
  async fetchConversation(client: Socket, { conversationId, ...data }) {
    try {
      const result = await this.socketIOService.fetchMemberConversation(String(client.data.id), data);

      return this.success(EventKey.FETCH_CONVERSATION, result);
    } catch (error) {
      this.logger.error(error);
      return socketFail(EventKey.FETCH_CONVERSATION, error);
    }
  }

  private success(chanel: string, data?: LiteralObject, payload = {}) {
    if (data?.paging) {
      delete data.paging;
      return { success: true, chanel, ...data, ...payload };
    }
    return { success: true, chanel, data, ...payload };
  }
}

/**
import { WebSocketGateway, SubscribeMessage, MessageBody, WebSocketServer } from '@nestjs/websockets';
import { SocketIOService } from './socket-io.service';
import { Socket, Server, Event as SocketEvent } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { Logger } from '@nestjs/common';

@WebSocketGateway()
export class SocketIOGateway {
  @WebSocketServer()
  server: Server;

  private readonly logger = new Logger(SocketIOGateway.name);

  constructor(private readonly socketIOService: SocketIOService, private readonly authService: AuthService) {}

  afterInit() {
    // TODO: Assign middleware verify token.
    console.log('Socket Inited');
  }

  async handleConnection(client: Socket) {
    // this.server.emit('message', 'huhu ahahaha');
    client.use(this.middlaware(client)).on('authenticated', (socket: Socket) => {
      console.log('Client connected', client.id);
      this.server.emit('notification', 'Success global message');
    });
  }

  middlaware(client: Socket) {
    return (event: SocketEvent, next: (err?: Error) => void) => {
      if (event[0] !== 'authenticate') return next();

      try {
        const token = event[1]?.token;
        const isValid = this.authService.verifyAccessToken(token);

        if (isValid) {
          client.emit('onConnected');
          event[0] = 'authenticated';
          return next();
        }

        client.emit('unauthorized', 'You was provided invalid token.');
      } catch (error) {
        this.logger.error(error);
        client.emit('unauthorized', error.message);
      }
    };
  }

  @SubscribeMessage('createChat')
  create(@MessageBody() createChatDto) {
    return this.socketIOService.create(createChatDto);
  }

  @SubscribeMessage('findAllChat')
  findAll() {
    return this.socketIOService.findAll();
  }

  @SubscribeMessage('findOneChat')
  findOne(@MessageBody() id: number) {
    return this.socketIOService.findOne(id);
  }

  @SubscribeMessage('updateChat')
  update(@MessageBody() updateChatDto) {
    return this.socketIOService.update(updateChatDto.id, updateChatDto);
  }

  @SubscribeMessage('removeChat')
  remove(@MessageBody() id: number) {
    return this.socketIOService.remove(id);
  }
}

 */
