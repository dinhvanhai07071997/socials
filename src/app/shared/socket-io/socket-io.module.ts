import { Module } from '@nestjs/common';
import { SocketIOService } from './socket-io.service';
import { AuthModule } from '../auth/auth.module';
import { SocketIOGateway } from './socket-io.gateway';
import { PublicGateway } from './public.gateway';
import { MessageModule } from '@libs/message';
import { MessageController } from './socket-io.controller';

@Module({
  imports: [AuthModule, MessageModule],
  providers: [SocketIOGateway, SocketIOService, PublicGateway],
  controllers: [MessageController],
})
export class SocketIOModule {}
