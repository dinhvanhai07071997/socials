import { UserData } from '$core/decorators/user.decorator';
import { assignLoadMore } from '$helpers/utils';
import { validate } from '$helpers/validate';
import { Body, Controller, Get, Param, Query, Req } from '@nestjs/common';
import { Request } from 'express';
import { SocketIOService } from './socket-io.service';

@Controller('message')
export class MessageController {
  constructor(private readonly socketIOService: SocketIOService) {}

  @Get('/conversations')
  async fetchConversation(@Req() req: Request) {
    assignLoadMore(req.query);

    const conversations = await this.socketIOService.fetchMemberConversation(String(req.user.id), req.query);
    return conversations;
  }

  @Get('/:conversationId')
  async getListPermissions(@Req() req: Request, @Param('conversationId') conversationId: string) {
    assignLoadMore(req.query);
    const messages = await this.socketIOService.fetchMessage(String(req.user.id), conversationId, req.query);
    return messages;
  }
}
