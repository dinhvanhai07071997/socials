import { LiteralObject } from '@nestjs/common';
import { MessageType } from '../enum';

export class SendMessageDto {
  conversationId: string;
  memberId: string;
  type: MessageType;
  content: string;
  payload: LiteralObject;
}
