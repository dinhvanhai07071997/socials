import { WebSocketGateway, SubscribeMessage, MessageBody, WebSocketServer } from '@nestjs/websockets';
import { SocketIOService } from './socket-io.service';
import { Socket, Server } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { Logger } from '@nestjs/common';

@WebSocketGateway({ namespace: '/public' })
export class PublicGateway {
  @WebSocketServer()
  server: Server;

  private readonly logger = new Logger(PublicGateway.name);

  constructor(private readonly socketIOService: SocketIOService, private readonly authService: AuthService) {}

  afterInit() {
    // TODO: Assign middleware verify token.

    console.log('Socket Inited');
  }

  async handleConnection(client: Socket) {
    this.server.emit('message', 'huhu ahahaha');
  }
}
