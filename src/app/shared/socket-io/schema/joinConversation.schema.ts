export const joinConversationSchema: AjvSchema = {
  type: 'object',
  required: ['id'],
  additionalProperties: false,
  properties: {
    id: {
      type: 'string',
      minLength: 1,
      format: 'objectId',
    },
  },
};
