import { MessageType } from '../enum';

export const sendMessageSchema: AjvSchema = {
  type: 'object',
  required: ['conversationId', 'type', 'content'],
  additionalProperties: false,
  properties: {
    conversationId: {
      type: 'string',
      minLength: 1,
      format: 'objectId',
    },
    type: {
      type: 'number',
      enum: [MessageType.TEXT],
    },
    content: {
      type: 'string',
      maxLength: 255,
      minLength: 1,
    },
    payload: {
      type: 'object',
    },
  },
};
