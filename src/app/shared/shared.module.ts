import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { GlobalCacheModule } from './cache/cache.module';
import { CronJobModule } from './cron-job/cron-job.module';
import { SocketIOModule } from './socket-io/socket-io.module';

@Module({
  imports: [AuthModule, GlobalCacheModule, CronJobModule, SocketIOModule],
  controllers: [],
  exports: [AuthModule],
})
export class SharedModule {}
