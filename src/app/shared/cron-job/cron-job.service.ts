import { Injectable } from '@nestjs/common';
// import { Logger } from '@nestjs/common';
// import { Cron, Interval, Timeout } from '@nestjs/schedule';

// https://docs.nestjs.com/techniques/task-scheduling
@Injectable()
export class CronJobService {
  //   private readonly logger = new Logger(CronJobService.name);
  //   @Cron('45 * * * * *', {
  //     name: 'notifications',
  //     timeZone: 'Asia/Tokyo',
  //   })
  //   handleCron() {
  //     this.logger.debug('Called when the current second is 45');
  //   }
  //   @Timeout('name', 5000)
  //   handleTimeout() {
  //     this.logger.debug('Called once after 5 seconds');
  //   }
  //   @Interval('name', 10000)
  //   handleInterval() {
  //     this.logger.debug('Called every 10 seconds');
  //   }
}
