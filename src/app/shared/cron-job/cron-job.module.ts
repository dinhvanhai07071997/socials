import { Module } from '@nestjs/common';
import { CronJobService } from './cron-job.service';

@Module({
  controllers: [],
  providers: [CronJobService],
})
export class CronJobModule {}
