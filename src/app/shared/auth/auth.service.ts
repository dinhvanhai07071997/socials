import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenType } from '$types/enums';
import config from '$config';
import { ITokenPayload } from '$types/interfaces';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  public generateAccessToken(payload: ITokenPayload): string {
    return this.jwtService.sign(
      { ...payload, tokenType: TokenType.ACCESS_TOKEN },
      {
        secret: config.AUTH.JWT_SECRET_KEY,
        expiresIn: config.AUTH.JWT_ACCESS_TOKEN_EXPIRES_IN,
      },
    );
  }

  public generateRefreshToken(payload: ITokenPayload): string {
    return this.jwtService.sign(
      { ...payload, tokenType: TokenType.REFRESH_TOKEN },
      {
        secret: config.AUTH.JWT_SECRET_KEY,
        expiresIn: config.AUTH.JWT_REFRESH_TOKEN_EXPIRES_IN,
      },
    );
  }

  public verifyAccessToken(accessToken: string) {
    try {
      const payload = this.jwtService.verify(accessToken, { secret: config.AUTH.JWT_SECRET_KEY });
      return payload?.tokenType === TokenType.ACCESS_TOKEN ? payload : false;
    } catch (error) {
      return false;
    }
  }

  public verifyRefreshToken(refreshToken: string) {
    try {
      const payload = this.jwtService.verify(refreshToken, { secret: config.AUTH.JWT_SECRET_KEY });
      return payload?.tokenType === TokenType.REFRESH_TOKEN ? payload : false;
    } catch (error) {
      return false;
    }
  }
}
