import config from '$config';
import { CacheModule, Global, Module } from '@nestjs/common';
import * as IORedisStore from 'cache-manager-ioredis';
import { GlobalCacheController } from './cache.controller';
import { GlobalCacheService } from './cache.service';

// https://docs.nestjs.com/techniques/caching
// https://github.com/BryanDonovan/node-cache-manager
@Global()
@Module({
  imports: [
    // CacheModule.register(),
    CacheModule.register({
      store: IORedisStore,
      socket: {
        host: config.REDIS.HOST,
        port: config.REDIS.PORT,
        db: config.REDIS.DB,
        password: config.REDIS.PASSWORD,
      },
      // one days
      ttl: 86400,
    }),
  ],
  controllers: [GlobalCacheController],
  providers: [GlobalCacheService],
  exports: [CacheModule],
})
export class GlobalCacheModule {}
