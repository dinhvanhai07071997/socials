import { Controller, Get } from '@nestjs/common';
import { GlobalCacheService } from './cache.service';

@Controller('cache')
export class GlobalCacheController {
  constructor(private readonly globalCacheService: GlobalCacheService) {}

  @Get('clear-all')
  async clearCache() {
    return this.globalCacheService.clearAllCache();
  }
}
