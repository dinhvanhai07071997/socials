import { Public } from '$core/decorators/public.decorator';
import { UserData } from '$core/decorators/user.decorator';
import { ClientGuard } from '$core/guards/client.guard';
import { validate } from '$helpers/validate';
import { Controller, Get, UseGuards, Request, Put, Body } from '@nestjs/common';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { updateProfileSchema } from './profile.schema';
import { ProfileService } from './profile.service';

@Controller('profile')
// @UseGuards(ClientGuard)
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Get('')
  getUserProfile(@UserData() user: Express.User) {
    return this.profileService.profile(user.id);
  }

  @Put('')
  updateProfile(@UserData() user: Express.User, @Body() body: UpdateProfileDto) {
    validate(updateProfileSchema, body);
    return this.profileService.updateProfile(user.id, body);
  }
}
