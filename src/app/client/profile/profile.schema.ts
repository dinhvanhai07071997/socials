import { MarrageStatus, UserGender } from '$types/enums';

export const updateProfileSchema: AjvSchema = {
  type: 'object',
  required: [],
  additionalProperties: false,
  properties: {
    firstName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
    avatar: {
      type: 'string',
    },
    banner: {
      type: 'string',
    },
    gender: {
      enum: [UserGender.FEMALE, UserGender.MALE, UserGender.OTHER],
    },
    marrageStatus: {
      enum: [MarrageStatus.DATING, MarrageStatus.MARRAGE, MarrageStatus.SINGLE],
    },
  },
};
