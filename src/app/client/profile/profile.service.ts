import { ClientGuard } from '$core/guards/client.guard';
import User from '$database/entities/User';
import { Exception } from '$helpers/exception';
import { assignThumbURL } from '$helpers/utils';
import { ErrorCode } from '$types/enums';
import { Injectable, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';

@Injectable()
export class ProfileService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async profile(userId: number) {
    const result = await this.userRepository.findOne({ id: userId });
    assignThumbURL(result, 'avatar');
    assignThumbURL(result, 'banner');
    return result;
  }

  async updateProfile(userId: number, body) {
    return await this.connection.transaction(async (transaction) => {
      const userRepository = transaction.getRepository(User);

      const user = await userRepository.findOne({ id: userId });
      if (!user) throw new Exception(ErrorCode.Not_Found, 'User not found');

      await userRepository.update(
        {
          id: userId,
        },
        {
          ...body,
        },
      );
      return true;
    });
  }
}
