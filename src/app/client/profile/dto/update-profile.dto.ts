import { MarrageStatus } from '$types/enums';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateProfileDto {
  avatar: string;
  banner: string;
  firstName: string;
  lastName: string;
  bio: string;
  address: string;
  marrageStatus: MarrageStatus;
}
