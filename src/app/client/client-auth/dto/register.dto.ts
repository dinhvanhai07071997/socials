import { UserGender } from '$types/enums';

export class RegisterDto {
  firstName: string;
  lastName: string;
  dob: string;
  gender: UserGender;
  email: string;
  password: string;
}
