export class LoginDto {
  phone: string;
  email: string;
  password: string;
}
