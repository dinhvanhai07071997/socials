export class ConfirmEmailDto {
  email: string;
  verifyCode: string;
}
