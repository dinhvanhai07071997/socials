export class ForgotPasswordDto {
  email: string;
  password: string;
}
