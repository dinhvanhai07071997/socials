import { CommonStatus, UserGender } from '$types/enums';

export const loginSchema: AjvSchema = {
  type: 'object',
  required: ['email', 'password'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },

    password: {
      type: 'string',
      minLength: 6,
    },
  },
};

export const registerSchema: AjvSchema = {
  type: 'object',
  required: ['email', 'password', 'firstName', 'lastName', 'gender', 'dob'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },
    firstName: {
      type: 'string',
      minLength: 1,
      maxLength: 127,
    },
    lastName: {
      type: 'string',
      minLength: 1,
      maxLength: 127,
    },
    password: {
      type: 'string',
      minLength: 6,
      maxLength: 32,
    },
    gender: {
      type: ['integer', 'null'],
      enum: [UserGender.FEMALE, UserGender.MALE, UserGender.OTHER, null],
    },
    dob: {
      type: 'string',
      format: 'date',
    },
  },
};

export const forgotPasswordSchema: AjvSchema = {
  type: 'object',
  required: ['email'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },
  },
};

export const changePasswordSchema: AjvSchema = {
  type: 'object',
  required: ['password', 'confirmPassword'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },
  },
};

export const resendMailSchema: AjvSchema = {
  type: 'object',
  required: ['email'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },
  },
};

export const confirmEmailSchema: AjvSchema = {
  type: 'object',
  required: ['email', 'verifyCode'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
    },
    verifyCode: {
      type: 'string',
    },
  },
};
