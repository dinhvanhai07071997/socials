import { AuthService } from '$app/shared/auth/auth.service';
import User from '$database/entities/User';

import { CommonStatus, ErrorCode, KeyQueue, OtpType, UserType, VerificationCodeStatus } from '$types/enums';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';

import VerificationCode from '$database/entities/VerificationCode';
import { Exception, Unauthorized } from '$helpers/exception';
import { compare, hash } from 'bcrypt';
import config from '$config';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { ResendEmailDto } from './dto/resend-mail.dto';
import { ConfirmEmailDto } from './dto/confirm-email.dto';
import { SendMailQueue } from '$helpers/queue';
import { isBeforeTime, randomOTP } from '$helpers/utils';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ejs = require('ejs');

const THREE_MINUTES = 3 * 60 * 1000;
@Injectable()
export class ClientAuthService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(VerificationCode)
    private readonly verificationRepository: Repository<VerificationCode>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly authService: AuthService,
  ) {}

  async login({ phone, email, password }: LoginDto) {
    let user: any = {};
    if (phone) {
      user = await this.userRepository.findOne({
        where: { phone },
        select: ['id', 'email', 'password', 'status'],
      });
    }

    if (email) {
      user = await this.userRepository.findOne({
        where: { email },
        select: ['id', 'email', 'password', 'status'],
      });
    }

    if (!user) throw new Exception(ErrorCode.Not_Found, 'This user does not exit');
    if (user.status === CommonStatus.INACTIVE) throw new Exception(ErrorCode.USER_INACTIVE);

    const isValid = await compare(password, user.password);
    if (!isValid) throw new Exception(ErrorCode.Email_Or_Password_Not_valid, 'Password incorrect.');

    return await this.generateToken(this.userRepository, user);
  }

  async register({ lastName, firstName, password, gender, dob, email }: RegisterDto) {
    return await this.connection.transaction(async (transaction) => {
      const userRepository = transaction.getRepository(User);
      const verificationCodeRepository = transaction.getRepository(VerificationCode);

      const isExist = await userRepository.findOne({
        where: {
          email,
        },
        select: ['id', 'email', 'status'],
      });

      if (isExist) throw new Exception(ErrorCode.Email_Already_Exist, 'This user have already existed. ');

      const hashedPassword = await hash(password, config.AUTH.BCRYPT_HASH_ROUNDS);
      const user = await userRepository.save({
        email,
        password: hashedPassword,
        lastName,
        firstName,
        dob,
        gender,
        status: CommonStatus.NOT_VERIFY,
      });

      const currentDate = new Date();
      const code = randomOTP();
      const verifyObj = await verificationCodeRepository.save({
        email,
        code,
        type: OtpType.REGISTER,
        expireAt: new Date(currentDate.getTime() + THREE_MINUTES),
        status: VerificationCodeStatus.ACTIVE,
      });

      const html = await ejs.renderFile('ejs/verificationMail.ejs', {
        user_firstname: firstName,
        code: verifyObj.code,
      });

      SendMailQueue.add(KeyQueue.SEND_MAIL, {
        receiver: email,
        subject: 'FaceBook',
        content: 'Confirm register',
        html: html,
      });

      return await this.generateToken(userRepository, user);
    });
  }

  async confirmEmail({ email, verifyCode }: ConfirmEmailDto) {
    return await this.connection.transaction(async (transaction) => {
      const userRepository = transaction.getRepository(User);
      const verificationCodeRepository = transaction.getRepository(VerificationCode);

      const user = await userRepository.findOne({ email });
      if (!user) throw new Exception(ErrorCode.Not_Found, 'This email does not exist!');

      if (user && user.status !== CommonStatus.NOT_VERIFY)
        throw new Exception(ErrorCode.user_have_been_activated, 'This user have been actived');

      const verification = await verificationCodeRepository.findOne({
        code: verifyCode,
        email,
        type: OtpType.REGISTER,
        status: VerificationCodeStatus.ACTIVE,
      });
      if (!verification) throw new Exception(ErrorCode.Not_Found, 'Verification code invalid');

      console.log(verification);

      await userRepository.update({ id: user.id }, { status: CommonStatus.ACTIVE });
      await verificationCodeRepository.update(
        { id: verification.id },
        {
          status: VerificationCodeStatus.USED,
        },
      );
    });
  }

  async forgotPassword(body: ForgotPasswordDto) {
    return '';
  }

  async resendMail({ email }: ResendEmailDto) {
    return await this.connection.transaction(async (transaction) => {
      const userRepository = transaction.getRepository(User);
      const verifyCodeRepository = transaction.getRepository(VerificationCode);

      const user = await userRepository.findOne({ email });
      if (!user) throw new Exception(ErrorCode.Not_Found, 'User not found');

      const currentDate = new Date();
      const code = randomOTP();
      const verification = await verifyCodeRepository.findOne({ email, type: OtpType.REGISTER });

      if (!verification) {
        await verifyCodeRepository.save({
          email,
          code,
          type: OtpType.REGISTER,
          expireAt: new Date(currentDate.getTime() + THREE_MINUTES),
        });
      }

      //TODO: update verification code and resend email
    });
  }

  /*
   * Generate token & refresh token.
   */
  async generateToken(userRepository: Repository<User>, user: User): Promise<{ token: string; refreshToken: string }> {
    const payload = { id: user.id, userType: UserType.CLIENT };

    const token = this.authService.generateAccessToken(payload);

    const isValid = this.authService.verifyRefreshToken(user.refreshToken);

    if (!isValid) {
      const newRefreshToken = this.authService.generateRefreshToken(payload);

      await userRepository.update(user.id, { refreshToken: newRefreshToken });

      return { token, refreshToken: newRefreshToken };
    }

    return { token, refreshToken: user.refreshToken };
  }

  async refreshToken(refreshToken: string): Promise<string> {
    const payload = this.authService.verifyRefreshToken(refreshToken);
    if (!payload) throw new Unauthorized('You have provided invalid refresh token');

    const user = await this.userRepository.findOne({
      where: { id: payload.id },
      select: ['id', 'refreshToken'],
    });

    if (refreshToken !== user.refreshToken) throw new Unauthorized('Your refresh token changed, please login again');

    const result = await this.generateToken(this.userRepository, user);
    return result?.token;
  }
}
