import { Public } from '$core/decorators/public.decorator';
import { Exception } from '$helpers/exception';
import { validate } from '$helpers/validate';
import { ErrorCode } from '$types/enums';
import { Body, Controller, Post } from '@nestjs/common';
import {
  confirmEmailSchema,
  forgotPasswordSchema,
  loginSchema,
  registerSchema,
  resendMailSchema,
} from './client-auth.schema';
import { ClientAuthService } from './client-auth.service';
import { ChangePasswordDto } from './dto/change-password-dto';
import { ConfirmEmailDto } from './dto/confirm-email.dto';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { ResendEmailDto } from './dto/resend-mail.dto';

@Controller('auth')
export class ClientAuthController {
  constructor(private readonly clientAuthService: ClientAuthService) {}

  @Public()
  @Post('/login')
  async login(@Body() body: LoginDto) {
    validate(loginSchema, body);
    return await this.clientAuthService.login(body);
  }

  @Public()
  @Post('/register')
  async register(@Body() body: RegisterDto) {
    validate(registerSchema, body);
    return await this.clientAuthService.register(body);
  }

  @Post('/confirm-email')
  async confirmEmail(@Body() body: ConfirmEmailDto) {
    validate(confirmEmailSchema, body);
    return await this.clientAuthService.confirmEmail(body);
  }

  @Public()
  @Post('/resend-email')
  async resendEmail(@Body() body: ResendEmailDto) {
    validate(resendMailSchema, body);
    return await this.resendEmail(body);
  }

  @Public()
  @Post('/forgot-password')
  async forgotPassword(@Body() body: ForgotPasswordDto) {
    validate(forgotPasswordSchema, body);
    return await this.clientAuthService.forgotPassword(body);
  }

  @Public()
  @Post('/change-password')
  async changePassword(@Body() body: ChangePasswordDto) {
    validate(forgotPasswordSchema, body);
    return await this.clientAuthService.forgotPassword(body);
  }

  @Public()
  @Post('/request-access-token')
  async refreshToken(@Body('refreshToken') refreshToken: string): Promise<{ token: string }> {
    validate({ type: 'string', minLength: 1 }, refreshToken);
    const token = await this.clientAuthService.refreshToken(refreshToken);
    return { token };
  }
}
