import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '$app/shared/auth/auth.module';
import { ClientAuthController } from './client-auth.controller';
import { ClientAuthService } from './client-auth.service';
import User from '$database/entities/User';
import VerificationCode from '$database/entities/VerificationCode';

@Module({
  imports: [TypeOrmModule.forFeature([VerificationCode, User]), AuthModule],
  controllers: [ClientAuthController],
  providers: [ClientAuthService],
})
export class ClientAuthModule {}
