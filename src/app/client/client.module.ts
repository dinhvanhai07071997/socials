import { Module } from '@nestjs/common';
import { ClientAuthModule } from './client-auth/client-auth.module';
import { NotificationModule } from './notification/notification.module';
import { ProfileModule } from './profile/profile.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [ClientAuthModule, ProfileModule, NotificationModule, UserModule],
  providers: [],
  controllers: [],
})
export class ClientModule {}
