export class SearchAllDto {
  keyword: string;
  pageIndex: number;
  pageSize: number;
  skip: number;
}
