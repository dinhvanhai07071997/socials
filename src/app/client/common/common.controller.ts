import { ClientGuard } from '$core/guards/client.guard';
import { assignPaging } from '$helpers/utils';
import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { CommonService } from './common.service';
import { SearchAllDto } from './dto/search-all.dto';

@Controller('common')
@UseGuards(ClientGuard)
export class CommonController {
  constructor(private readonly commonService: CommonService) {}

  @Get()
  getAll(@Query() query: SearchAllDto) {
    assignPaging(query);
    return this.commonService.searchAll(query);
  }
}
