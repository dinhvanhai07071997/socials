import User from '$database/entities/User';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { SearchAllDto } from './dto/search-all.dto';

@Injectable()
export class CommonService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async searchAll(params: SearchAllDto) {
    // const queryBuilder = this.userRepository.createQueryBuilder('');
  }
}
