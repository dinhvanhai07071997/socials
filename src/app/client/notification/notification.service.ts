import Notification from '$database/entities/Notification';
import NotificationUser from '$database/entities/NotificationUser';
import { NotificationTargetType, NotificationType } from '$types/enums';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ListNotificationDto } from './dto/list-notification.dto';

@Injectable()
export class NotificationService {
  constructor(
    @InjectRepository(Notification)
    private readonly notificationRepository: Repository<Notification>,

    @InjectRepository(NotificationUser)
    private readonly notificationUserRepository: Repository<NotificationUser>,
  ) {}

  async getListNotifications(memberId: number, params: ListNotificationDto) {
    const queryBuilder = await this.notificationRepository
      .createQueryBuilder('n')
      .leftJoinAndMapOne('n.notificationUser', NotificationUser, 'nu', ' n.id = nm.notificationId')
      .andWhere('nu.userId = :userId OR n.targetType = :targetType', {
        memberId,
        targetType: NotificationTargetType.ALL,
      })
      .select(['n.id', 'n.title', 'n.image', 'n.targetType', 'nu.notificationId', 'nu.memberId']);
  }
}
