export class ListNotificationDto {
  takeAfter: string;
  pageSize: number;
}
