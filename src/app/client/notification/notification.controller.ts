import { UserData } from '$core/decorators/user.decorator';
import { assignLoadMore } from '$helpers/utils';
import { Controller, Get, Query } from '@nestjs/common';
import { ListNotificationDto } from './dto/list-notification.dto';
import { NotificationService } from './notification.service';

@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}
  @Get('/')
  getListNotifications(@UserData() user: Express.User, @Query() params: ListNotificationDto) {
    assignLoadMore(params);
    return this.notificationService.getListNotifications(user.id, params);
  }
}
