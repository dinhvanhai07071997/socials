import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { NotificationController } from './notification.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import Notification from '$database/entities/Notification';
import NotificationMember from '$database/entities/NotificationUser';

@Module({
  imports: [TypeOrmModule.forFeature([Notification, NotificationMember])],
  controllers: [NotificationController],
  providers: [NotificationService],
})
export class NotificationModule {}
