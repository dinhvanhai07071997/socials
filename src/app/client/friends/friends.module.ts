import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '$app/shared/auth/auth.module';
import { FriendService } from './friends.service';
import User from '$database/entities/User';
import VerificationCode from '$database/entities/VerificationCode';
import { FriendsController } from './friends.controller';

@Module({
  imports: [TypeOrmModule.forFeature([VerificationCode, User]), AuthModule],
  controllers: [FriendsController],
  providers: [FriendService],
})
export class FriendsModule {}
