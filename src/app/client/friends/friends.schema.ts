export const addFriendSchema: AjvSchema = {
  type: 'object',
  required: ['targetId'],
  additionalProperties: false,
  properties: {
    targetId: {
      type: 'number',
    },
  },
};
