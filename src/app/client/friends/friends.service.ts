import User from '$database/entities/User';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { AddFrienDto } from './dto/add-friends.dto';

@Injectable()
export class FriendService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async addFriends(userId: number, body: AddFrienDto) {
    return '';
  }
}
