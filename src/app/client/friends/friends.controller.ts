import { UserData } from '$core/decorators/user.decorator';
import { validate } from '$helpers/validate';
import { Body, Controller, Post } from '@nestjs/common';
import { AddFrienDto } from './dto/add-friends.dto';
import { addFriendSchema } from './friends.schema';
import { FriendService } from './friends.service';

@Controller('friends')
export class FriendsController {
  constructor(private readonly friendService: FriendService) {}

  @Post('/add-friends')
  async addFriend(@UserData() user: Express.User, @Body() body: AddFrienDto) {
    validate(addFriendSchema, body);

    return await this.friendService.addFriends(user.id, body);
  }
}
