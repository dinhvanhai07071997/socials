export const updateProfileSchema: AjvSchema = {
  type: 'object',
  required: ['firstName', 'lastName', 'address', 'dob', 'address', 'gender'],
  additionalProperties: false,
  properties: {
    firstName: {
      type: 'string',
    },
  },
};
