import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import User from '$database/entities/User';
import Image from '$database/entities/Image';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import NotificationUser from '$database/entities/NotificationUser';

@Module({
  imports: [TypeOrmModule.forFeature([User, Image, NotificationUser])],
  controllers: [UserController],
  providers: [UserService],
  exports: [],
})
export class UserModule {}
