import { ClientGuard } from '$core/guards/client.guard';
import User from '$database/entities/User';
import { assignThumbURL } from '$helpers/utils';
import { Injectable, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { GetListUserByIdDto } from './dto/get-list-user-by-id.dto';
import NotificationUser from '$database/entities/NotificationUser';
import { CommonStatus } from '$types/enums';

@Injectable()
export class UserService {
  constructor(
    private readonly connection: Connection,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(NotificationUser)
    private readonly notificationUserRepository: Repository<NotificationUser>,
  ) {}

  async profile(userId: number) {
    const result = await this.userRepository.findOne({ id: userId });
    assignThumbURL(result.avatar, 'avatar');
    return result;
  }

  async updateProfile(userId: number, body) {
    return await this.connection.transaction(async (transaction) => {
      const userRepository = transaction.getRepository(User);

      return 'Haireus';
    });
  }

  async getListUserById(params: GetListUserByIdDto) {
    const queryBuilder = await this.userRepository
      .createQueryBuilder('u')
      .leftJoinAndMapMany('u.notificationUser', NotificationUser, 'nu', ' u.id = nu.userId AND nu.isRead = :isRead', {
        isRead: CommonStatus.INACTIVE,
      })
      .where('u.id IN (:userIds)', { userIds: params.userIds })
      .select(['u.id', 'u.firstName', 'u.lastName', 'u.avatar', 'nu.isRead', 'nu.notificationId']);

    const result = await queryBuilder.orderBy('u.id', 'DESC').getMany();
    assignThumbURL(result, 'avatar');
    return result;
  }
}
