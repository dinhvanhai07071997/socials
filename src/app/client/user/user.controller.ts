import { Public } from '$core/decorators/public.decorator';
import { UserData } from '$core/decorators/user.decorator';
import { ClientGuard } from '$core/guards/client.guard';
import { Controller, Get, UseGuards, Request, Put, Body, Query } from '@nestjs/common';
import { query } from 'express';
import { GetListUserByIdDto } from './dto/get-list-user-by-id.dto';
import { UserService } from './user.service';

@Controller('user')
@UseGuards(ClientGuard)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/list-by-id')
  @Public()
  findAll(@Query() query: GetListUserByIdDto) {
    return this.userService.getListUserById(query);
  }
}
