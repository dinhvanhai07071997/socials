import { AdminGuard } from '$core/guards/admin.guard';
import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { AdminAuthModule } from './admin-auth/admin-auth.module';

@Module({
  imports: [AdminAuthModule],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AdminGuard,
    },
  ],
  exports: [AdminAuthModule],
})
export class AdminModule {}
