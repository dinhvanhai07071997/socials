const io = require('socket.io-client');

let token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlclR5cGUiOiJTVVBFUl9BRE1JTiIsInRva2VuVHlwZSI6IkFDQ0VTU19UT0tFTiIsImlhdCI6MTY0NTM0MDIwNywiZXhwIjoxNjQ3OTMyMjA3fQ.7E7vrb_gW5y4EJJa8xOrmASfUFR3ly1geE0wGj1JxTs`;
let fake = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9a.eyJpZCI6MSwidXNlclR5cGUiOiJTVVBFUl9BRE1JTiIsInRva2VuVHlwZSI6IkFDQ0VTU19UT0tFTiIsImlhdCI6MTY0NTM0MDIwNywiZXhwIjoxNjQ3OTMyMjA3fQ.7E7vrb_gW5y4EJJa8xOrmASfUFR3ly1geE0wGj1JxTs`;

let socket = io(`http://localhost:3000`, {
  extraHeaders: { authorization: token },
});

socket.on('connect_error', (err) => {
  console.log(`connect_error due to ${err.message}`);

  if (err.message === 'Unauthorized') {
    console.log('Your access token is no longer active.');
  }
});

socket.on('connect', () => {
  console.log('onConnected');

  socket.emit('joinConversation', { id: '6212075c2ec22033bf156444' }, (data) => {
    if (data.success) {
      // const message = {
      //   conversationId: '6212075c2ec22033bf156444',
      //   type: 1,
      //   content: 'Em an com chua?',
      //   payload: {},
      // };
      // Send message
      // socket.emit('sendMessage', message, (data) => console.log('sendMessage', data));

      // Fetch message
      // socket.emit('fetchMessage', { conversationId: '6212075c2ec22033bf156444', takeAfter: null }, (data) =>

      // );

      // Fetch conversations
      // socket.emit('fetchConversation', { takeAfter: null }, (data) => console.log('fetchConversation', data.data[0]));

      // Fetch
      socket.emit('getOrCreateP2PConversation', { targetId: '6' }, (data) =>
        console.log('getOrCreateP2PConversation', data.data),
      );
    }
  });
});

socket.on('notification', (data) => {
  console.log(data);
});

socket.on('message', (data) => {
  console.log('onMessage', data);
});

socket.on('conversation', (data) => {
  console.log('onConversation', data);
});

socket.on('exception', (data) => {
  console.log('exception', data);
});

socket.on('error', (data) => {
  console.log('error', data);
});
